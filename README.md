# Qt Automotive Suite - Android Builds

Builds the following Qt Automotive Suite components (branch 5.13):
- [Qt Application Manager](https://doc.qt.io/QtApplicationManager/)
- [Qt IVI](https://doc.qt.io/QtIVI/)

For:
- Android `x86`
- Android `x86_64`

Using:
- Android NDK r19c
- Android SDK 28
- Qt 5.13.0

Check the `ci-docker` branch for more details about the build environment.

## Install

*TODO*
