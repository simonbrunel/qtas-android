#!/bin/bash

QT_BRANCH=5.13
QT_VERSION=5.13.0
QT_REPO_URL=git://code.qt.io/qt
ANDROID_NDK_TOOLCHAIN_PREFIX=${BUILD_ARCH}
ANDROID_NDK_TOOLS_PREFIX=i686-linux-android
ZIP_SUFFIX=linux-${QT_VERSION}-android-${BUILD_ARCH}.zip
QMAKE=${QT_PATH}/${QT_VERSION}/android_${BUILD_ARCH}/bin/qmake

CI_ROOT_DIR=`pwd`
QTAS_ROOT_DIR=/tmp/qtas

mkdir -p ${QTAS_ROOT_DIR}

build_qt_module()
{
    MOD_NAME=$1
    MOD_PRO_FILE=$2
    MOD_GIT_URL=${QT_REPO_URL}/${MOD_NAME}.git
    MOD_ROOT_DIR=${QTAS_ROOT_DIR}/${MOD_NAME}
    MOD_SRC_DIR=${MOD_ROOT_DIR}/src
    MOD_BUILD_DIR=${MOD_ROOT_DIR}/build
    MOD_INSTALL_DIR=${MOD_ROOT_DIR}/dist
    MOD_ZIP_FILE=${CI_ROOT_DIR}/qtas-${MOD_NAME}-${ZIP_SUFFIX}

    rm -rf ${MOD_BUILD_DIR}
    rm -rf ${MOD_INSTALL_DIR}
    mkdir -p ${MOD_SRC_DIR}
    mkdir -p ${MOD_BUILD_DIR}
    mkdir -p ${MOD_INSTALL_DIR}

    pushd ${MOD_BUILD_DIR}
    git clone --recursive --single-branch --branch ${QT_BRANCH} ${MOD_GIT_URL} ${MOD_SRC_DIR}
    ${QMAKE} ${MOD_SRC_DIR}/${MOD_PRO_FILE} -spec android-clang CONFIG+=qtquickcompiler
    make -j4 qmake_all 1>/dev/null
    make -j4 install INSTALL_ROOT=${MOD_INSTALL_DIR} 1>/dev/null
    make -j4 install
    popd

    pushd ${MOD_INSTALL_DIR}/./${QT_PATH}
    zip -q -r ${MOD_ZIP_FILE} .
    popd
}

build_qt_module qtapplicationmanager application-manager.pro
build_qt_module qtivi qtivi.pro
